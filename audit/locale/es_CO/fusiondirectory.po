# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2018
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2019-03-21 14:04+0000\n"
"PO-Revision-Date: 2018-08-13 19:50+0000\n"
"Last-Translator: fusiondirectory <contact@fusiondirectory.org>, 2018\n"
"Language-Team: Spanish (Colombia) (https://www.transifex.com/fusiondirectory/teams/12202/es_CO/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: es_CO\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: admin/audit/class_auditEvent.inc:29 admin/audit/class_auditEvent.inc:33
msgid "Audit event"
msgstr ""

#: admin/audit/class_auditEvent.inc:30
msgid "An event like ldap modification which was registered by audit plugin"
msgstr ""

#: admin/audit/class_auditEvent.inc:50
msgid "Event"
msgstr ""

#: admin/audit/class_auditEvent.inc:54
msgid "Time"
msgstr ""

#: admin/audit/class_auditEvent.inc:54
msgid "Date and time this event happened"
msgstr ""

#: admin/audit/class_auditEvent.inc:57
msgid "Action"
msgstr ""

#: admin/audit/class_auditEvent.inc:57
msgid "Action type"
msgstr ""

#: admin/audit/class_auditEvent.inc:58
msgid "Author"
msgstr ""

#: admin/audit/class_auditEvent.inc:58
msgid "Action author"
msgstr ""

#: admin/audit/class_auditEvent.inc:59
msgid "Object type"
msgstr ""

#: admin/audit/class_auditEvent.inc:60
msgid "Object"
msgstr "Objeto"

#: admin/audit/class_auditEvent.inc:60
msgid "Target object"
msgstr ""

#: admin/audit/class_auditEvent.inc:61
msgid "Attributes"
msgstr ""

#: admin/audit/class_auditEvent.inc:61
msgid "Target attributes"
msgstr ""

#: admin/audit/class_auditEvent.inc:62
msgid "Result"
msgstr ""

#: admin/audit/class_auditEvent.inc:62
msgid "Result or error"
msgstr ""

#: admin/audit/class_auditManagement.inc:40
#: config/audit/class_auditConfig.inc:40
msgid "Audit"
msgstr ""

#: admin/audit/class_auditManagement.inc:41
msgid "Audit events"
msgstr ""

#: admin/audit/class_auditManagement.inc:42
msgid "Browse audit events"
msgstr ""

#: admin/audit/class_auditManagement.inc:53
msgid "Date"
msgstr ""

#: config/audit/class_auditConfig.inc:26
msgid "Audit configuration"
msgstr ""

#: config/audit/class_auditConfig.inc:27
msgid "FusionDirectory audit plugin configuration"
msgstr ""

#: config/audit/class_auditConfig.inc:43
msgid "Audit RDN"
msgstr ""

#: config/audit/class_auditConfig.inc:43
msgid "Branch in which audit events will be stored"
msgstr ""

#: config/audit/class_auditConfig.inc:49
msgid "Actions to audit"
msgstr ""

#: config/audit/class_auditConfig.inc:49
msgid "Which actions should be stored in LDAP audit log"
msgstr ""

#: config/audit/class_auditConfig.inc:56
msgid "Days to keep"
msgstr ""

#: config/audit/class_auditConfig.inc:56
msgid "Number of days of audit to keep in the LDAP when cleaning"
msgstr ""
