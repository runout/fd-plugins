# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2019-03-21 14:04+0000\n"
"PO-Revision-Date: 2018-08-13 19:55+0000\n"
"Language-Team: Spanish (Colombia) (https://www.transifex.com/fusiondirectory/teams/12202/es_CO/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: es_CO\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: admin/systems/ipmi/class_ipmiClient.inc:28
msgid "IPMI client"
msgstr ""

#: admin/systems/ipmi/class_ipmiClient.inc:29
msgid "Edit IPMI client settings"
msgstr ""

#: admin/systems/ipmi/class_ipmiClient.inc:41
msgid "IPMI client settings"
msgstr ""

#: admin/systems/ipmi/class_ipmiClient.inc:44
msgid "IP"
msgstr ""

#: admin/systems/ipmi/class_ipmiClient.inc:44
msgid "IP of the IPMI interface"
msgstr ""

#: admin/systems/ipmi/class_ipmiClient.inc:48
msgid "User login"
msgstr ""

#: admin/systems/ipmi/class_ipmiClient.inc:48
msgid "IPMI user login"
msgstr ""

#: admin/systems/ipmi/class_ipmiClient.inc:53
msgid "User password"
msgstr ""

#: admin/systems/ipmi/class_ipmiClient.inc:53
msgid "IPMI user password"
msgstr ""
