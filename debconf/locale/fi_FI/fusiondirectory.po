# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2018
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2019-03-21 14:04+0000\n"
"PO-Revision-Date: 2018-08-13 19:51+0000\n"
"Last-Translator: fusiondirectory <contact@fusiondirectory.org>, 2018\n"
"Language-Team: Finnish (Finland) (https://www.transifex.com/fusiondirectory/teams/12202/fi_FI/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: fi_FI\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: admin/debconfProfile/class_debconfProfileManagement.inc:32
#: admin/systems/debconf/class_debconfStartup.inc:31
msgid "Debconf"
msgstr ""

#: admin/debconfProfile/class_debconfProfileManagement.inc:33
msgid "Debconf profile management"
msgstr ""

#: admin/debconfProfile/class_debconfProfileGeneric.inc:71
#: admin/debconfProfile/class_debconfProfileGeneric.inc:85
msgid "Error"
msgstr "Virhe"

#: admin/debconfProfile/class_debconfProfileGeneric.inc:71
msgid "There is no template for this profile"
msgstr ""

#: admin/debconfProfile/class_debconfProfileGeneric.inc:86
#, php-format
msgid "Can't find entry %s in LDAP for profile %s"
msgstr ""

#: admin/debconfProfile/class_debconfProfileGeneric.inc:138
msgid "LDAP error"
msgstr "LDAP virhe"

#: admin/debconfProfile/class_debconfProfileGeneric.inc:141
msgid "LDIF error"
msgstr ""

#: admin/debconfProfile/class_debconfProfileGeneric.inc:151
#, php-format
msgid ""
"In order to import a debconf file, please run the following command : "
"<br/><i>debconf2ldif.pl -b ou=<b>name</b>,%s -k <b>filename</b> > "
"template.ldif </i><br/>With <b>filename</b> the file name, and <b>name</b> "
"the desired name for the template.<br/>"
msgstr ""

#: admin/debconfProfile/class_debconfProfileGeneric.inc:163
#: admin/debconfProfile/class_debconfProfileGeneric.inc:168
msgid "Debconf profile"
msgstr ""

#: admin/debconfProfile/class_debconfProfileGeneric.inc:164
msgid "Debconf profile information"
msgstr ""

#: admin/debconfProfile/class_debconfProfileGeneric.inc:182
#: admin/debconfProfile/class_debconfProfileGeneric.inc:185
#: admin/debconfProfile/debconfProfile-filter.tpl.c:5
msgid "Name"
msgstr "Nimi"

#: admin/debconfProfile/class_debconfProfileGeneric.inc:184
msgid "Import a debconf file"
msgstr ""

#: admin/debconfProfile/class_debconfProfileGeneric.inc:185
msgid "Name of this debconf template"
msgstr ""

#: admin/debconfProfile/class_debconfProfileGeneric.inc:189
msgid "Entries"
msgstr ""

#: admin/debconfProfile/class_debconfProfileGeneric.inc:191
msgid "Debconf template answers"
msgstr ""

#: admin/debconfProfile/class_debconfProfileGeneric.inc:206
msgid "Import"
msgstr ""

#: admin/systems/debconf/class_debconfStartup.inc:32
msgid "Debconf preseed startup"
msgstr ""

#: admin/systems/debconf/class_debconfStartup.inc:45
msgid "Debconf settings"
msgstr ""

#: admin/systems/debconf/class_debconfStartup.inc:48
msgid "Profile"
msgstr ""

#: admin/systems/debconf/class_debconfStartup.inc:48
msgid "Debconf preseed profile to be used for installation"
msgstr ""

#: admin/systems/debconf/class_debconfStartup.inc:52
msgid "Release"
msgstr ""

#: admin/systems/debconf/class_debconfStartup.inc:52
msgid "Debian release to install"
msgstr ""

#: admin/debconfProfile/debconfProfile-filter.tpl.c:2
msgid "Filter"
msgstr ""
